import { useState } from 'react'

const App = () => {
  const [r, setR] = useState({ current: 0, total: 0 })
  const [cycles, setCycles] = useState({ current: 0, total: 0 })

  const fib = (n1 = 0, n2 = 1, c = cycles.current) => {
    let newN1
    while (c > 0) {
      newN1 = n2
      n2 = n1 + n2
      n1 = newN1
      c--
    }
    setR({
      current: n1 + n2,
      total: n1 + n2 + r.total,
    })
  }

  return (
    <>
      {cycles.current}
      <input
        type="range"
        min="1"
        max="10"
        value={cycles.current}
        onChange={(ev) => {
          setCycles({
            current: Number(ev.target.value),
            total: cycles.total + Number(ev.target.value),
          })
        }}
      />
      <button
        onClick={() => {
          fib()
          setCycles({
            current: cycles.current,
            total: cycles.current + cycles.total,
          })
          console.log('current:', cycles.current, 'total:', cycles.total)
        }}
      >
        [ rabbits: {r.current} ] [ total: {r.total} ]
      </button>
    </>
  )
}

export default App
